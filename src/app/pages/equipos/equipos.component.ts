import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.component.html',
  styleUrls: ['./equipos.component.scss'],
})
export class EquiposComponent implements OnInit {

  constructor( public menuadmin: MenuController,
               public menuprincipal: MenuController, ) { }

  ngOnInit() {}
  openMenu() {
    console.log('open menu');
    this.menuadmin.toggle('menuAdmin');
  }

  menuPrin() {
    console.log('menu principal');
    this.menuprincipal.toggle('menuPrincipal');

  }

}
