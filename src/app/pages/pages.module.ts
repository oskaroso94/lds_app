import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { EquiposComponent } from './equipos/equipos.component';
import { partitionArray } from '@angular/compiler/src/util';
import { PartidosComponent } from './partidos/partidos.component';
import { SancionesComponent } from './sanciones/sanciones.component';
import { ContactosComponent } from './contactos/contactos.component';
import { TabPosicionesComponent } from './tab-posiciones/tab-posiciones.component';
import { IonicModule } from '@ionic/angular';





@NgModule({
  declarations: [
    HomeComponent,
    EquiposComponent,
    PartidosComponent,
    SancionesComponent,
    ContactosComponent,
    TabPosicionesComponent,


  ],
  imports: [
    CommonModule,
    IonicModule,


  ]
})
export class PagesModule { }
