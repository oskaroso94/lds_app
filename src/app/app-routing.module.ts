import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { EquiposComponent } from './pages/equipos/equipos.component';
import { PartidosComponent } from './pages/partidos/partidos.component';
import { SancionesComponent } from './pages/sanciones/sanciones.component';
import { ContactosComponent } from './pages/contactos/contactos.component';
import { RgstDirigenteComponent } from './backend/rgst-dirigente/rgst-dirigente.component';
import { RgstJugadoresComponent } from './backend/rgst-jugadores/rgst-jugadores.component';
import { CreacionEquiposComponent } from './backend/creacion-equipos/creacion-equipos.component';
import { PlubNoticiasComponent } from './backend/plub-noticias/plub-noticias.component';
import { PlubPartidosComponent } from './backend/plub-partidos/plub-partidos.component';
import { TabPosicionesComponent } from './pages/tab-posiciones/tab-posiciones.component';
import { TablPosicionesComponent } from './backend/tabl-posiciones/tabl-posiciones.component';
import { PlubSancionesComponent } from './backend/plub-sanciones/plub-sanciones.component';

const routes: Routes = [
  // direccionamiento de paginas
  { path: 'home', component: HomeComponent },
  { path: 'equipos', component: EquiposComponent },
  { path: 'partidos', component: PartidosComponent },
  { path: 'tabla de posiciones', component: TabPosicionesComponent },
  { path: 'sanciones', component: SancionesComponent },
  { path: 'contactos', component: ContactosComponent },


  // direccionamiento de paginas backend
  { path: 'registro-Dirigentes', component: RgstDirigenteComponent },
  { path: 'registro-jugadores', component: RgstJugadoresComponent },
  { path: 'creacion-equipos', component: CreacionEquiposComponent },
  { path: 'publicacion-noticias', component: PlubNoticiasComponent },
  { path: 'publicacion-tabla', component: TablPosicionesComponent },
  { path: 'publicacion-partidos', component: PlubPartidosComponent },
  { path: 'publicacion-sanciones', component: PlubSancionesComponent },

  { path: '', component: HomeComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
