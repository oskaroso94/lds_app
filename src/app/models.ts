

export interface Dirigente {
    foto: string;
    nombre: string;
    equipos: string;
    telf1: number;
    telf2: number;
    id: string;
    fecha: Date;
    categoria: string;
 }


export interface Equipo {
     logo: string;
     nombre: string;
     categoria: string;
     id: string;
 }

export interface Jugador {
    foto: string;
    nombre: string;
    equipo: string;
    cedula: number;
    numero: number;
    apodo: string;
    posicionJuego: string;
}

export interface Sancion {
    fecha: Date;
    jugador: string;
    equipos: string;
    tiempo: string;
    descripcion: string;
}

export interface Partido {
    fecha: Date;
    mesa: string;
    central: string;
    asistente1: string;
    asistente2: string;
    equipo1: string;
    jugadores1: string;
    goles1: number;
    equipo2: string;
    jugadores2: string;
    goles2: number;
    informe: string;
}

export interface Noticia {
    titulo: string;
    foto: string;
    descripcion: string;
}

export interface Tabla {
    logo:string;
    nombreequipo: string;
    pjugado: number;
    pganado: number;
    pempatado: number;
    pperdido: number;
    gfavor: number;
    gcontra: number;
    puntos: number;
    gdiferencia: number;

}
