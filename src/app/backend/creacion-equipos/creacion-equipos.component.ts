
import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { FirestorageService } from '../../services/firestorage.service';
import { Equipo } from '../../models';

@Component({
  selector: 'app-creacion-equipos',
  templateUrl: './creacion-equipos.component.html',
  styleUrls: ['./creacion-equipos.component.scss'],
})
export class CreacionEquiposComponent implements OnInit {

  enableNewequipo = false;

  newEquipo: Equipo;

  Equipos: Equipo [] = [];

  private path = 'Equipos/';

  newImage = '';

  newfile = '';

  loading: any;

  constructor( public menuEquipos: MenuController,
               public menuadmin: MenuController,
               public menuprincipal: MenuController,
               public firestoreservice: FirestoreService,
               public firestorageService: FirestorageService,
               ) { }

  ngOnInit() {}

  openMenu() {
    this.menuadmin.toggle('menuAdmin');
  }

  menuPrin() {
    this.menuprincipal.toggle('menuPrincipal');

  }

  nuevoequipo() {
    this.enableNewequipo = true;
    this.newEquipo = {
      logo: '',
      nombre: '',
      categoria: '',
      id: this.firestoreservice.getId(),
    };
  }
}
