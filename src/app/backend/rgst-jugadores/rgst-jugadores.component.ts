import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-rgst-jugadores',
  templateUrl: './rgst-jugadores.component.html',
  styleUrls: ['./rgst-jugadores.component.scss'],
})
export class RgstJugadoresComponent implements OnInit {

  constructor(public menuJugadores: MenuController,
              public menuadmin: MenuController,
              public menuprincipal: MenuController, ) { }

ngOnInit() {}


  openMenu() {
    console.log('open menu');
    this.menuadmin.toggle('menuAdmin');
  }

  menuPrin() {
    console.log('menu principal');
    this.menuprincipal.toggle('menuPrincipal');

  }
}
