import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-plub-sanciones',
  templateUrl: './plub-sanciones.component.html',
  styleUrls: ['./plub-sanciones.component.scss'],
})
export class PlubSancionesComponent implements OnInit {

  constructor( public menuadmin: MenuController,
               public menuprincipal: MenuController, ) { }

  ngOnInit() {}

  openMenu() {
    console.log('open menu');
    this.menuadmin.toggle('menuAdmin');
  }

  menuPrin() {
    console.log('menu principal');
    this.menuprincipal.toggle('menuPrincipal');

  }

}
