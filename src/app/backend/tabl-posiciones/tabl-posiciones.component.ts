import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-tabl-posiciones',
  templateUrl: './tabl-posiciones.component.html',
  styleUrls: ['./tabl-posiciones.component.scss'],
})
export class TablPosicionesComponent implements OnInit {

  constructor( public menuadmin: MenuController,
               public menuprincipal: MenuController, ) { }

  ngOnInit() {}

  openMenu() {
    console.log('open menu');
    this.menuadmin.toggle('menuAdmin');
  }

  menuPrin() {
    console.log('menu principal');
    this.menuprincipal.toggle('menuPrincipal');

  }

}
