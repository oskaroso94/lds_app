import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreacionEquiposComponent } from './creacion-equipos/creacion-equipos.component';
import { PlubPartidosComponent } from './plub-partidos/plub-partidos.component';
import { PlubSancionesComponent } from './plub-sanciones/plub-sanciones.component';
import { PlubNoticiasComponent } from './plub-noticias/plub-noticias.component';
import { RgstJugadoresComponent } from './rgst-jugadores/rgst-jugadores.component';
import { RgstDirigenteComponent } from './rgst-dirigente/rgst-dirigente.component';
import { TablPosicionesComponent } from './tabl-posiciones/tabl-posiciones.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CreacionEquiposComponent,
    PlubPartidosComponent,
    PlubSancionesComponent,
    PlubNoticiasComponent,
    RgstJugadoresComponent,
    RgstDirigenteComponent,
    TablPosicionesComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,

  ]
})
export class BackendModule { }
