

import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, MenuController, ToastController } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { Dirigente } from '../../models';
import { FirestorageService } from '../../services/firestorage.service';


@Component({
  selector: 'app-rgst-dirigente',
  templateUrl: './rgst-dirigente.component.html',
  styleUrls: ['./rgst-dirigente.component.scss'],
})
export class RgstDirigenteComponent implements OnInit {

  dirigentes: Dirigente [] = [];

  newDirigente: Dirigente;

  enableNewDirigente = false;

  private path = 'Dirigentes/';

  newImage = '';

  newfile = '';

  loading: any;



  constructor(public menuadmin: MenuController,
              public menuprincipal: MenuController,
              public firestoreservice: FirestoreService,
              public loadingController: LoadingController,
              public toastController: ToastController,
              public alertController: AlertController,
              public firestorageService: FirestorageService,

    ) { }

  ngOnInit() {
    this.getDirigentes();
  }

  openMenu() {
    console.log('open menu');
    this.menuadmin.toggle('menuAdmin');
  }

  menuPrin() {
    console.log('menu principal');
    this.menuprincipal.toggle('menuPrincipal');

  }

  async guardardirigente(){
    this.presentLoading();
    const path = 'Dirigentes';
    const name = this.newDirigente.nombre;
    const res = await this.firestorageService.uploadImage(this.newfile, path, name);
    this.newDirigente.foto = res;
    this.firestoreservice.createDoc(this.newDirigente, this.path, this.newDirigente.id).then( res => {
        this.loading.dismiss();
        this.presentToast('Guardando con Exito');
    }).catch( error => {
      this.presentToast('Error al guardar!');
    });
  }

  getDirigentes() {
    this.firestoreservice.getCollection<Dirigente>(this.path).subscribe( res => {
      this.dirigentes = res;
    } );
  }

  async deleteDirigente(dirigente: Dirigente) {

    const alert = await this.alertController.create({
      cssClass: 'normal',
      header: 'Advertencia',
      message: 'Eliminar <strong>Dirigente</strong>!!!',
      buttons: [
        {
          text: 'Cacelar',
          role: 'cancel',
          // cssClass: 'normal',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.firestoreservice.deleteDoc(this.path, dirigente.id).then( res => {
              this.presentToast('Dirigente Eliminado');
              this.alertController.dismiss();
          }).catch(error => {
            this.presentToast('Error al Eliminar!');
          });
          }
        }
      ]
    });

    await alert.present();

  }

  nuevodir() {
    this.enableNewDirigente = true;
    this.newDirigente = {
      foto: '',
      nombre: '',
      equipos: '',
      categoria: '',
      telf1: null,
      telf2: null,
      id: this.firestoreservice.getId(),
      fecha: new Date(),

    };
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: 'normal',
      message: 'Guardando...',
    });
    await this.loading.present();
    // await loading.onDidDismiss();
   // console.log('Loading dismissed!');
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      cssClass: 'normal',
      duration: 2000,
      color: 'danger',
    });
    toast.present();
  }

  async newImageUpload(event: any) {
    // console.log(event);
      if (event.target.files && event.target.files[0]) {
        this.newfile = event.target.files[0];
        const reader = new FileReader();
        reader.onload = ((image) => {
           this.newImage = image.target.result as string;
      });
        reader.readAsDataURL(event.target.files[0]);
      }

  }



}


