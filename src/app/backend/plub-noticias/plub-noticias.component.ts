import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-plub-noticias',
  templateUrl: './plub-noticias.component.html',
  styleUrls: ['./plub-noticias.component.scss'],
})
export class PlubNoticiasComponent implements OnInit {

  constructor(  public menuadmin: MenuController,
                public menuprincipal: MenuController, ) { }

  ngOnInit() {}

  openMenu() {
    console.log('open menu');
    this.menuadmin.toggle('menuAdmin');
  }

  menuPrin() {
    console.log('menu principal');
    this.menuprincipal.toggle('menuPrincipal');

  }

}
