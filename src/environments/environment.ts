// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebaseConfig : {
    apiKey: "AIzaSyD0FMTDzWpu0lGf--pmpFWwtJA5TRVHst0",
    authDomain: "lds-fut.firebaseapp.com",
    databaseURL: "https://lds-fut.firebaseio.com",
    projectId: "lds-fut",
    storageBucket: "lds-fut.appspot.com",
    messagingSenderId: "520093061657",
    appId: "1:520093061657:web:f9f26100ac80e8d5a85088",
    measurementId: "G-D69CDRZ7CK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
